from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
import urllib
import json
import random

from .models import Reply
from urllib import request

WEBHOOK_URL = 'https://hooks.slack.com/services/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'
VERIFICATION_TOKEN = 'EpNr3TUIoOLCCX4a5WwSFXXR'
ACTION_HOW_ARE_YOU = 'HOW_ARE_YOU'
ACTION_DO_YOU_LIKE = 'DO YOU LIKE'

def index(request):
    positive_replies = Reply.objects.filter(response=Reply.POSITIVE)
    neutral_replies = Reply.objects.filter(response=Reply.NEUTRAL)
    negative_replies = Reply.objects.filter(response=Reply.NEGATIVE)
    apple_replies = Reply.objects.filter(response=Reply.APPLE)
    orange_replies = Reply.objects.filter(response=Reply.ORANGE)
    grape_replies = Reply.objects.filter(response=Reply.GRAPE)
    
    context = {
        'positive_replies': positive_replies,
        'neutral_replies': neutral_replies,
        'negative_replies': negative_replies,
        'apple_replies': apple_replies,
        'orange_replies': orange_replies,
        'grape_replies': grape_replies,
    }
    return render(request, 'index.html', context)

def clear(request):
    Reply.objects.all().delete()
    return redirect(index)

def announce(request):
    if request.method == 'POST':
        data = {
            'text': request.POST['message']
        }
        post_message(WEBHOOK_URL, data)

    return redirect(index)

@csrf_exempt
def echo(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'text': '<@{}> {}'.format(user_id, content.upper()),
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

@csrf_exempt
def hello(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'blocks': [
            {
                'type' : 'section',
                'text' : {
                    'type': 'mrkdwn',
                    'text': '<@{}> How are you?'.format(user_id)
                },
                'accessory': {
                    'type': 'static_select',
                    'placeholder': {
                        'type': 'plain_text',
                        'text': 'I am:',
                        'emoji': True
                    },
                    'options': [
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Fine.',
                                'emoji': True
                            },
                            'value': 'positive'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'So so.',
                                'emoji': True
                            },
                            'value': 'neutral'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Terrible.',
                                'emoji': True
                            },
                            'value': 'negative'
                        }
                    ],
                    'action_id': ACTION_HOW_ARE_YOU
                }
            }
        ],
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

@csrf_exempt
def reply(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    payload = json.loads(request.POST.get('payload'))
    print(payload)
    if payload.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user = payload['user']
    selected_value = payload['actions'][0]['selected_option']['value']
    response_url = payload['response_url']

    if payload['actions'][0]['action_id'] == ACTION_HOW_ARE_YOU:
        if selected_value == 'positive':
            reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.POSITIVE)
            reply.save()
            response = {
                'text': '<@{}> Great! :smile:'.format(user['id'])
            }
        elif selected_value == 'neutral':
            reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEUTRAL)
            reply.save()
            response = {
                'text': '<@{}> Ok, thank you! :sweat_smile:'.format(user['id'])
         }
        else:
            reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEGATIVE)
            reply.save()
            response = {
                'text': '<@{}> Good luck! :innocent:'.format(user['id'])
            }

    elif payload['actions'][0]['action_id'] == ACTION_DO_YOU_LIKE:
        if selected_value == 'apple':
            reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.APPLE)
            reply.save()
            response = {
                'text': '<@{}> Apple is delicious! '.format(user['id'])
            }
        elif selected_value == 'orange':
            reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.ORANGE)
            reply.save()
            response = {
                'text': '<@{}> Orange is nice! '.format(user['id'])
            }
        else:
            reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.GRAPE)
            reply.save()
            response = {
                'text': '<@{}> Grape is good!'.format(user['id'])
            }
    
    else:
        raise SuspiciousOperation('Invalid request.')
    
    
    post_message(response_url, response)

    return JsonResponse({})

def post_message(url, data):
    headers = {
        'Content-Type': 'application/json',
    }
    req = urllib.request.Request(url, json.dumps(data).encode(), headers)
    with urllib.request.urlopen(req) as res:
        body = res.read()

@csrf_exempt
def omikuzi(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']
    
    s=random.randint(0,5)
    result0 = {
        'blocks':[
            {
                'type':'section',
                'text':{
                    'type': 'mrkdwn',
                    'text': '<@{}>大吉'.format(user_id)
                }
            },
        ],
        'response_type':'in_channel'
    }
    result1 = {
        'blocks':[
            {
                'type':'section',
                'text':{
                    'type': 'mrkdwn',
                    'text':'<@{}>中吉'.format(user_id)
                }
            }
        ],
        'response_type':'in_channel'
    }

    result2= {
        'blocks':[
            {
                'type':'section',
                'text' : {
                    'type' : 'mrkdwn',
                    'text':'<@{}>小吉'.format(user_id)
                },
            }
        ],
        'response_type':'in_channel'
    }
    result3 = {
        'blocks':[
            {
                'type':'section',
                'text':{
                    'type': 'mrkdwn',
                    'text':'<@{}>吉'.format(user_id)
                }
            }
        ],
        'response_type':'in_channel'
    }
    result4 = {
        'blocks':[
            {
                'type':'section',
                'text':{
                    'type': 'mrkdwn',
                    'text':'<@{}>凶'.format(user_id)
                }
            }
        ],
        'response_type':'in_channel'
    }
    result5 = {
        'blocks':[
            {
                'type':'section',
                'text':{
                    'type': 'mrkdwn',
                    'text':'<@{}>大凶'.format(user_id)
                }
            }
        ],
        'response_type':'in_channel'
    }
    if (s==0):
        return JsonResponse(result0)
    elif (s==1):
        return JsonResponse(result1)
    elif (s==2):
        return JsonResponse(result2)
    elif (s==3):
        return JsonResponse(result3)
    elif (s==4):
        return JsonResponse(result4)
    else:
        return JsonResponse(result5)

@csrf_exempt
def weather(requestt):

    if requestt.method != 'POST':
        return JsonResponse({})

    if requestt.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    user_name = requestt.POST['user_name']
    user_id = requestt.POST['user_id']
    content = requestt.POST['text']

    API_KEY = "c617efbb510ec94954a036536502c590"
    WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?lat=35.7802941&lon=139.7133576&APPID=c617efbb510ec94954a036536502c590"
    
    req = request.Request(WEATHER_URL)
    res = request.urlopen(req)
    json_body = json.loads(res.read() .decode('utf-8'))
    today_weather = json_body['weather'][0]['main']
    temp_k = json_body['main']['temp']
    temp_c = temp_k - 273.15
    
    result = {
        'blocks': [
            {
                'type' : 'section',
                'text' : {
                    'type': 'mrkdwn',
                    'text': '<@{}> weather:{}, temp:{:.4g}℃'.format(user_id, today_weather, temp_c),
                }
            }
        ],
        'response_type': 'in_channel'
    }

    res.close()

    return JsonResponse(result)

@csrf_exempt
def fruits(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'blocks': [
            {
                'type' : 'section',
                'text' : {
                    'type': 'mrkdwn',
                    'text': '<@{}> What fruits do you like?'.format(user_id)
                },
                'accessory': {
                    'type': 'static_select',
                    'placeholder': {
                        'type': 'plain_text',
                        'text': 'Fruits:',
                    },
                    'options': [
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Apple',
                            },
                            'value': 'apple'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Orange',
                            },
                            'value': 'orange'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Grape',
                            },
                            'value': 'grape'
                        }
                    ],
                    'action_id': ACTION_DO_YOU_LIKE
                }
            }
        ],
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

